/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  server
Filename: test_engine.h
Purpose:  External CSC Header
*/

#ifndef TEST_ENGINE_H
#define TEST_ENGINE_H
#include <common.h>
#include <test_engine_const.h>
#include <test_engine_types.h>

enum Common_status_code init_test_engine(void* arg);
enum Common_status_code teardown_test_engine(void* arg);

bool test_engine_comparison(enum Test_engine_comparison_mode mode,
                            enum Test_engine_comparison_type type,
                            char*                            file_name,
                            uint64_t                         file_line,
                            int64_t                          ix,
                            int64_t                          iy,
                            uint64_t                         ux,
                            uint64_t                         uy,
                            double                           fx,
                            double                           fy);

#define ASSERT_I_EQ(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_EQ,                                                              \
                               TEST_ENGINE_COMPARE_SIGNED_INT,                                                         \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_I_NE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_NE,                                                              \
                               TEST_ENGINE_COMPARE_SIGNED_INT,                                                         \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_I_LT(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_LT,                                                              \
                               TEST_ENGINE_COMPARE_SIGNED_INT,                                                         \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_I_GT(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_GT,                                                              \
                               TEST_ENGINE_COMPARE_SIGNED_INT,                                                         \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_I_LE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_LE,                                                              \
                               TEST_ENGINE_COMPARE_SIGNED_INT,                                                         \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_I_GE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_GE,                                                              \
                               TEST_ENGINE_COMPARE_SIGNED_INT,                                                         \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }

#define ASSERT_U_EQ(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_EQ,                                                              \
                               TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                       \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_U_NE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_NE,                                                              \
                               TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                       \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_U_LT(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_LT,                                                              \
                               TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                       \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_U_GT(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_GT,                                                              \
                               TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                       \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_U_LE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_LE,                                                              \
                               TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                       \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_U_GE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_GE,                                                              \
                               TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                       \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y,                                                                                      \
                               0,                                                                                      \
                               0) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }

#define ASSERT_F_EQ(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_EQ,                                                              \
                               TEST_ENGINE_COMPARE_FLOAT,                                                              \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_F_NE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_NE,                                                              \
                               TEST_ENGINE_COMPARE_FLOAT,                                                              \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_F_LT(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_LT,                                                              \
                               TEST_ENGINE_COMPARE_FLOAT,                                                              \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_F_GT(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_GT,                                                              \
                               TEST_ENGINE_COMPARE_FLOAT,                                                              \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_F_LE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_LE,                                                              \
                               TEST_ENGINE_COMPARE_FLOAT,                                                              \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }
#define ASSERT_F_GE(x, y)                                                                                              \
    if (test_engine_comparison(TEST_ENGINE_COMPARISON_GE,                                                              \
                               TEST_ENGINE_COMPARE_FLOAT,                                                              \
                               __FILE__,                                                                               \
                               __LINE__,                                                                               \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               0,                                                                                      \
                               x,                                                                                      \
                               y) == false) {                                                                          \
        *test_ok = false;                                                                                              \
        return;                                                                                                        \
    }

#define EXPECT_I_EQ(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_EQ,                                                       \
                                      TEST_ENGINE_COMPARE_SIGNED_INT,                                                  \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_I_NE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_NE,                                                       \
                                      TEST_ENGINE_COMPARE_SIGNED_INT,                                                  \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_I_LT(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_LT,                                                       \
                                      TEST_ENGINE_COMPARE_SIGNED_INT,                                                  \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_I_GT(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_GT,                                                       \
                                      TEST_ENGINE_COMPARE_SIGNED_INT,                                                  \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_I_LE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_LE,                                                       \
                                      TEST_ENGINE_COMPARE_SIGNED_INT,                                                  \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_I_GE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_GE,                                                       \
                                      TEST_ENGINE_COMPARE_SIGNED_INT,                                                  \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;

#define EXPECT_U_EQ(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_EQ,                                                       \
                                      TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_U_NE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_NE,                                                       \
                                      TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_U_LT(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_LT,                                                       \
                                      TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_U_GT(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_GT,                                                       \
                                      TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_U_LE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_LE,                                                       \
                                      TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_U_GE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_GE,                                                       \
                                      TEST_ENGINE_COMPARE_UNSIGNED_INT,                                                \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y,                                                                               \
                                      0,                                                                               \
                                      0)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;

#define EXPECT_F_EQ(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_EQ,                                                       \
                                      TEST_ENGINE_COMPARE_FLOAT,                                                       \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_F_NE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_NE,                                                       \
                                      TEST_ENGINE_COMPARE_FLOAT,                                                       \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_F_LT(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_LT,                                                       \
                                      TEST_ENGINE_COMPARE_FLOAT,                                                       \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_F_GT(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_GT,                                                       \
                                      TEST_ENGINE_COMPARE_FLOAT,                                                       \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_F_LE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_LE,                                                       \
                                      TEST_ENGINE_COMPARE_FLOAT,                                                       \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;
#define EXPECT_F_GE(x, y)                                                                                              \
    *test_ok = test_engine_comparison(TEST_ENGINE_COMPARISON_GE,                                                       \
                                      TEST_ENGINE_COMPARE_FLOAT,                                                       \
                                      __FILE__,                                                                        \
                                      __LINE__,                                                                        \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      0,                                                                               \
                                      x,                                                                               \
                                      y)                                                                               \
                 ? *test_ok                                                                                            \
                 : false;

/* Give this a signature so that any C static analysis is happy with a symbol */
#define TEST(name) void test_engine_##name(bool* test_ok)

#endif

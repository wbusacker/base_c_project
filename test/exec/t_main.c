/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  flight_control
Filename: main.c
Purpose:  Program entry point
*/

#include <stddef.h>
#include <test_engine.h>

int main(int argc, char** argv) {

    init_test_engine(NULL);

    teardown_test_engine(NULL);
}
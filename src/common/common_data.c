/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  flight_control
Filename: common_data.c
Purpose:  CSC data definition
*/

#include <common_data.h>

const char* common_status_code_strings[COMMON_STATUS_MAX_CODES + 1] = {"OK",
                                                                       "INITIALIZATION_ERROR",
                                                                       "SHUTDOWN_REQUESTED",
                                                                       "CYCLE_ERROR",
                                                                       "UNKNOWN_CODE"};
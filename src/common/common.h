/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  flight_control
Filename: common.h
Purpose:  Common definitions utilized across the entire program
*/

#ifndef COMMON_H
#define COMMON_H
#include <common_const.h>
#include <common_types.h>

enum Common_status_code init_common(void* arg);
enum Common_status_code teardown_common(void* arg);

const char* common_get_status_code_string(enum Common_status_code code);

#endif

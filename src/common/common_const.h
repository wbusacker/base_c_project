/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  flight_control
Filename: common_const.h
Purpose:  CSC constants
*/

#ifndef COMMON_CONST_H
#define COMMON_CONST_H

#define COMMON_CYCLE_PERIOD_NS (10000)

/* Don't forget to update the string codes in _data.c */
enum Common_status_code {
    COMMON_STATUS_OK,
    COMMON_STATUS_INITIALIZATION_ERROR,
    COMMON_STATUS_SHUTDOWN_REQUESTED,
    COMMON_STATUS_CYCLE_ERROR,
    COMMON_STATUS_MAX_CODES
};

#endif

/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  flight_control
Filename: common_get_status_code_string.c
Purpose:  Return the string to the status code provided
*/

#include <common_functions.h>

const char* common_get_status_code_string(enum Common_status_code code) {

    enum Common_status_code lookup = code;
    if (code > COMMON_STATUS_MAX_CODES) {
        lookup = COMMON_STATUS_MAX_CODES;
    }

    return common_status_code_strings[lookup];
}
/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  flight_control
Filename: common_types.h
Purpose:  CSC data types
*/

#ifndef COMMON_TYPES_H
#define COMMON_TYPES_H
#include <common_const.h>
#include <stdbool.h>
#include <stdint.h>

struct Common_csc_interface {

    struct {
        enum Common_status_code (*function)(void* arg);
        void* arguments;
    } init;

    struct {
        enum Common_status_code (*function)(void* arg);
        void* arguments;
    } teardown;

    struct {
        enum Common_status_code (*function)(uint64_t cycle_count, void* arg);
        void* arguments;
    } cycle;

    bool initialized;

    uint64_t cycle_count;
    uint64_t total_cycle_execution_time_ns;
    uint64_t minimum_cycle_execution_time_ns;
    uint64_t maximum_cycle_execution_time_ns;
};

#endif

/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  flight_control
Filename: common.c
Purpose:  CSC data and initialization definitions
*/

#include <common.h>
#include <common_functions.h>
#include <string.h>

enum Common_status_code init_common(void* arg) {

    return COMMON_STATUS_OK;
}

enum Common_status_code teardown_common(void* arg) {

    return COMMON_STATUS_OK;
}

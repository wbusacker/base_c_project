/*
Copyright (c) 2023 Will Busacker
See project license for more details

Project:  tcars
Filename: main.c
Purpose:  Program entry point
*/

/* System Headers */
#include <stddef.h>

/* P3X CSCs */
#include <log.h>
#include <rate_monotonic.h>
#include <uptime.h>

/* Project CSCs*/
#include <common.h>

int main(int argc, char** argv) {

    /* Always initialize the log first thing */
    init_log(argv[0]);
    log_set_global_level(LOG_LEVEL_DEBUG);

    // clang-format off

    struct Common_csc_interface cscs[] = {
        {.init = {.function = init_common, .arguments = NULL}, .teardown = {.function = teardown_common, .arguments = NULL}, .cycle = {.function = NULL, .arguments = NULL}},
    };

    // clang-format on

    uint64_t CSC_COUNT = sizeof(cscs) / sizeof(struct Common_csc_interface);

    /* First mark all CSCs as uninitialized */
    for (uint64_t csc = 0; csc < CSC_COUNT; csc++) {
        cscs[csc].initialized                     = false;
        cscs[csc].cycle_count                     = 0;
        cscs[csc].total_cycle_execution_time_ns   = 0;
        cscs[csc].minimum_cycle_execution_time_ns = 0xFFFFFFFFFFFFFFFF;
        cscs[csc].maximum_cycle_execution_time_ns = 0;
    }

    /* Attempt initialization of all CSCs */
    bool all_init = true;
    for (uint64_t csc = 0; csc < CSC_COUNT; csc++) {
        cscs[csc].initialized          = true;
        enum Common_status_code status = cscs[csc].init.function(cscs[csc].init.arguments);

        if (status != COMMON_STATUS_OK) {
            LOG_ALWAYS("CSC %d init failed: %s", csc, common_get_status_code_string(status));
            all_init = false;
            break;
        }
    }

    if (all_init) {

        struct Rate_monotonic_t rm;

        rate_monotonic_init(&rm, COMMON_CYCLE_PERIOD_NS, RATE_MONOTONIC_IMMEDIATE_UNLOCK);

        /* Main execution of the program */
        bool     keep_cycling = true;
        uint64_t cycle_count  = 0;

        while (keep_cycling) {

            rate_monotonic_cycle(&rm);
            for (uint64_t csc = 0; csc < CSC_COUNT; csc++) {

                if (cscs[csc].cycle.function != NULL) {
                    uint64_t                time_start = uptime_nanoseconds();
                    enum Common_status_code status     = cscs[csc].cycle.function(cycle_count, NULL);
                    uint64_t                time_stop  = uptime_nanoseconds();
                    uint64_t                delta_time = time_stop - time_start;

                    cscs[csc].cycle_count++;
                    cscs[csc].total_cycle_execution_time_ns += delta_time;

                    if (delta_time > cscs[csc].maximum_cycle_execution_time_ns) {
                        cscs[csc].maximum_cycle_execution_time_ns = delta_time;
                    }

                    if (delta_time < cscs[csc].minimum_cycle_execution_time_ns) {
                        cscs[csc].minimum_cycle_execution_time_ns = delta_time;
                    }

                    if (status != COMMON_STATUS_OK) {
                        keep_cycling = false;
                        LOG_ALWAYS("CSC %d cycle returned not ok: %s\n", csc, common_get_status_code_string(status));
                    }
                }
            }

            cycle_count++;
        }
    }

    /* Teardown all CSCS in reverse order */
    for (int64_t csc = CSC_COUNT; csc >= 0; csc--) {

        if (cscs[csc].initialized) {
            enum Common_status_code status = cscs[csc].teardown.function(NULL);

            if (status != COMMON_STATUS_OK) {
                LOG_ALWAYS("CSC %d init failed: %s\n", csc, common_get_status_code_string(status));
                all_init = false;
                break;
            }

            if (cscs[csc].cycle.function != NULL) {
                LOG_ALWAYS("CSC %d Cycle Count %ld Execution Times : Max %12dns Min %12dns Avg %12dns\n",
                           csc,
                           cscs[csc].cycle_count,
                           cscs[csc].maximum_cycle_execution_time_ns,
                           cscs[csc].minimum_cycle_execution_time_ns,
                           cscs[csc].total_cycle_execution_time_ns / cscs[csc].cycle_count);
            }
        }
    }

    teardown_log();

    return 0;
}
